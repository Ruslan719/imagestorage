use ImageStorage
go

insert dbo.AspNetRoles
(
	Name,
	NormalizedName,
	ConcurrencyStamp
)
values
(
	N'Admin',
	N'ADMIN',
	lower(newid())
) 

declare @role_id int = scope_identity(),
	@user_id int = (select top 1 Id from dbo.AspNetUsers order by Id)

insert dbo.AspNetUserRoles(UserId,RoleId)
values (@user_id, @role_id) 