﻿using ImageStorage.DomainModel.DataModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ImageStorage.DomainModel.Repositories
{
	/// <summary>
	/// Интерфейс репозитория картинок.
	/// </summary>
	public interface IImageRepository
	{
		/// <summary>
		/// Возвращает картинку.
		/// </summary>
		/// <param name="id">Идентификатор картинки.</param>
		/// <returns>Картинка.</returns>
		Task<Image> GetAsync(int id);

		/// <summary>
		/// Возвращает список картинок, принадлежащих пользователю.
		/// </summary>
		/// <param name="user_id">Идентификатор пользователя.</param>
		/// <returns>Список картинок.</returns>
		Task<List<Image>> GetByUserIdAsync(int user_id);

		/// <summary>
		/// Добавляет картинки.
		/// </summary>
		/// <param name="images">Перечисление картинок.</param>
		Task AddRangeAsync(IEnumerable<Image> images);

		/// <summary>
		/// Изменяет картинку.
		/// </summary>
		/// <param name="image">Картинка.</param>
		Task UpdateAsync(Image image);

		/// <summary>
		/// Удаляет картинку.
		/// Если указанной картинки нет, не выбрасывает исключение.
		/// </summary>
		/// <param name="image">Картинка.</param>
		Task RemoveAsync(Image image);
	}
}