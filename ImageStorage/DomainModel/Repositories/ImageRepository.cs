﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ImageStorage.DomainModel.DataModels;
using ImageStorage.DomainModel.DbContexts;
using Microsoft.EntityFrameworkCore;

namespace ImageStorage.DomainModel.Repositories
{
	/// <summary>
	/// Репозиторий картинок.
	/// </summary>
	public class ImageRepository : IImageRepository
	{
		/// <summary>
		/// Контекст взаимодействия с БД.
		/// </summary>
		private readonly MainIdentityContext _context;

		/// <summary>
		/// Инициализирует поля.
		/// </summary>
		/// <param name="context">Контекст взаимодействия с БД.</param>
		public ImageRepository(MainIdentityContext context)
		{
			_context = context;
		}

		/// <summary>
		/// Возвращает картинку.
		/// </summary>
		/// <param name="id">Идентификатор картинки.</param>
		/// <returns>Картинка.</returns>
		public Task<Image> GetAsync(int id) =>
			_context.Images
				.Include(i => i.User)
				.FirstOrDefaultAsync(image => image.Id == id);

		/// <summary>
		/// Возвращает список картинок, принадлежащих пользователю.
		/// </summary>
		/// <param name="user_id">Идентификатор пользователя.</param>
		/// <returns>Список картинок.</returns>
		public Task<List<Image>> GetByUserIdAsync(int user_id) =>
			_context.Images
				.Include(i => i.User)
				.Where(image => image.UserId == user_id)
				.ToListAsync();

		/// <summary>
		/// Добавляет картинки.
		/// </summary>
		/// <param name="images">Перечисление картинок.</param>
		public Task AddRangeAsync(IEnumerable<Image> images)
		{
			// Не предполагается добавление большого количества объектов.
			_context.Images.AddRange(images);
			return _context.SaveChangesAsync();
		}

		/// <summary>
		/// Изменяет картинку.
		/// </summary>
		/// <param name="image">Картинка.</param>
		public Task UpdateAsync(Image image)
		{
			_context.Images.Update(image);
			return _context.SaveChangesAsync();
		}

		/// <summary>
		/// Удаляет картинку.
		/// Если указанной картинки нет, не выбрасывает исключение.
		/// </summary>
		/// <param name="image">Картинка.</param>
		public Task RemoveAsync(Image image)
		{
			_context.Images.Remove(image);

			return _context.SaveChangesAsync();
		}
	}
}