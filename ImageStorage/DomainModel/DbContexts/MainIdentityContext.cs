﻿using ImageStorage.DomainModel.DataModels;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace ImageStorage.DomainModel.DbContexts
{
	/// <summary>
	/// Контекст взаимодействия с БД.
	/// </summary>
	public class MainIdentityContext : IdentityDbContext<User, IdentityRole<int>, int>
	{
		/// <summary>
		/// Точка доступа к картинкам.
		/// </summary>
		public DbSet<Image> Images { get; set; }

		/// <summary>
		/// Инициализирует и настраивает контекст.
		/// </summary>
		/// <param name="options">Настройки.</param>
		public MainIdentityContext(DbContextOptions<MainIdentityContext> options) : base(options)
		{
		}

		/// <summary>
		/// Обрабатывает событие создания модели.
		/// </summary>
		/// <param name="model_builder">Строитель модели.</param>
		protected override void OnModelCreating(ModelBuilder model_builder)
		{
			base.OnModelCreating(model_builder);

			model_builder.Entity<Image>()
				.HasOne(e => e.User)
				.WithMany(s => s.Images)
				.OnDelete(DeleteBehavior.Cascade);

			model_builder.Entity<Image>()
				.Property(e => e.ContentType)
				.HasConversion<int>();
		}
	}
}