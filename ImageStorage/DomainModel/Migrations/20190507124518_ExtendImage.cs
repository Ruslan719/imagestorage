﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ImageStorage.DomainModel.Migrations
{
	/// <summary>
	/// Расширение таблицы картинок новыми полями. Внимание! Удаляет все картинки.
	/// </summary>
	public partial class ExtendImage : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
			migrationBuilder.Sql("truncate table Images");

			migrationBuilder.AddColumn<int>(
                name: "ContentType",
                table: "Images",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<DateTime>(
                name: "InsertDt",
                table: "Images",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<bool>(
                name: "IsPublic",
                table: "Images",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<long>(
                name: "Length",
                table: "Images",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<string>(
                name: "Name",
                table: "Images",
                nullable: false,
                defaultValue: "");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ContentType",
                table: "Images");

            migrationBuilder.DropColumn(
                name: "InsertDt",
                table: "Images");

            migrationBuilder.DropColumn(
                name: "IsPublic",
                table: "Images");

            migrationBuilder.DropColumn(
                name: "Length",
                table: "Images");

            migrationBuilder.DropColumn(
                name: "Name",
                table: "Images");
        }
    }
}
