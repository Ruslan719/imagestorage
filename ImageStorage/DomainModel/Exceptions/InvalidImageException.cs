﻿using System;

namespace ImageStorage.DomainModel.Exceptions
{
	/// <summary>
	/// Исключение при не успешной валидации картинки.
	/// </summary>
	public class InvalidImageException : Exception
	{
		public override string Message => "Incorrect image.";
	}
}