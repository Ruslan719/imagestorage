﻿using System;

namespace ImageStorage.DomainModel.Exceptions
{
	/// <summary>
	/// Исключение при неудачной попытки найти ресурс.
	/// </summary>
	public class NotFoundException : Exception
	{
		/// <summary>
		/// Инициализирует исключение.
		/// </summary>
		/// <param name="message">Сообщение.</param>
		public NotFoundException(string message) : base(message)
		{
		}
	}
}