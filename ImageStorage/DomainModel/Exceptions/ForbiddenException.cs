﻿using System;

namespace ImageStorage.DomainModel.Exceptions
{
	/// <summary>
	/// Исключение при отсутствии прав на совершение действия.
	/// </summary>
	public class ForbiddenException : Exception
	{
		/// <summary>
		/// Инициализирует исключение.
		/// </summary>
		/// <param name="message">Сообщение.</param>
		public ForbiddenException(string message) : base(message)
		{
		}
	}
}