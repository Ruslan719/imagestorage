﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ImageStorage.DomainModel.DataModels;
using ImageStorage.DomainModel.Exceptions;
using ImageStorage.DomainModel.Repositories;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;

namespace ImageStorage.DomainModel.Services
{
	/// <summary>
	/// Сервис картинок.
	/// </summary>
	/// <typeparam name="T">Тип пользователя.</typeparam>
	public class ImageService<T> : IImageService<T> where T : IdentityUser<int>, new()
	{
		/// <summary>
		/// Название конфигурационной переменной максимального размера картинки в байтах.
		/// </summary>
		public const string MAX_IMAGE_LENGTH_SECTION = "MaxImageLength";

		/// <summary>
		/// Допустимые типы картинок.
		/// </summary>
		public static readonly HashSet<ContentType> AllowedContentTypes = new HashSet<ContentType>
		{
			ContentType.ImageBmp,
			ContentType.ImagePng,
			ContentType.ImageJpeg
		};

		/// <summary>
		/// Репозиторий картинок.
		/// </summary>
		private readonly IImageRepository _imageRepository;

		/// <summary>
		/// Сервис пользователей.
		/// </summary>
		private readonly IUserService<T> _userService;

		/// <summary>
		/// Точка доступа к конфигурации.
		/// </summary>
		private readonly IConfiguration _configuration;

		/// <summary>
		/// Инициализирует поля.
		/// </summary>
		/// <param name="image_repository">Репозиторий картинок.</param>
		/// <param name="user_service">Сервис пользователей.</param>
		/// <param name="configuration">Точка доступа к конфигурации.</param>
		public ImageService(IImageRepository image_repository, IUserService<T> user_service, IConfiguration configuration)
		{
			_imageRepository = image_repository;
			_userService = user_service;
			_configuration = configuration;
		}

		/// <summary>
		/// Добавляет картинки.
		/// </summary>
		/// <param name="image_files">Список файлов.</param>
		public async Task AddRangeAsync(List<File> image_files)
		{
			if (!image_files.All(IsValid))
			{
				throw new InvalidImageException();
			}

			T user = await _userService.GetCurrentAsync();
			if (user == null)
			{
				throw new NotFoundException("Current user was not found.");
			}

			IEnumerable<Image> images = image_files.Select(f => new Image
			{
				UserId = user.Id,
				Base64Data = f.Base64Data,
				Length = f.Length,
				ContentType = f.ContentType,
				InsertDt = DateTime.Now,
				Name = f.Name
			});

			await _imageRepository.AddRangeAsync(images);
		}

		/// <summary>
		/// Изменяет картинку.
		/// Если картинки с указанным идентификатором нет, выбрасывает исключение.
		/// Изменить картинку может только автор.
		/// </summary>
		/// <param name="id">Идентификатор картинки.</param>
		/// <param name="is_public">Имеет публичный доступ.</param>
		public async Task UpdateAsync(int id, bool is_public)
		{
			Image image = await GetAsync(id);
			if (image == null)
			{
				throw new NotFoundException("Image was not found.");
			}
			
			if (await HasFullAccessAsync(image.UserId))
			{
				image.IsPublic = is_public;
				await _imageRepository.UpdateAsync(image);
			}
			else
			{
				throw new ForbiddenException("You do not have sufficient access to update image.");
			}
		}

		/// <summary>
		/// Возвращает картинку.
		/// </summary>
		/// <param name="id">Идентификатор картинки.</param>
		/// <returns>Картинка.</returns>
		public Task<Image> GetAsync(int id) => _imageRepository.GetAsync(id);

		/// <summary>
		/// Возвращает список картинок, принадлежащих пользователю.
		/// Если не указан идентификатор текущего пользователя, вернет список публичных картинок. 
		/// </summary>
		/// <param name="user_id">Идентификатор пользователя.</param>
		/// <returns>Список картинок.</returns>
		public async Task<List<Image>> GetByUserIdAsync(int user_id)
		{
			// TODO Optimize (remove repository, return IQueryable)?
			List<Image> images = await _imageRepository.GetByUserIdAsync(user_id);

			return await HasFullAccessAsync(user_id) ? images : images.Where(i => i.IsPublic).ToList();
		}

		/// <summary>
		/// Удаляет картинку.
		/// Если картинки с указанным идентификатором нет, не выбрасывает исключение.
		/// Удалить картинку может только автор.
		/// </summary>
		/// <param name="id">Идентификатор картинки.</param>
		public async Task RemoveAsync(int id)
		{
			Image image = await GetAsync(id);
			if (image == null)
			{
				return;
			}
			
			if (await HasFullAccessAsync(image.UserId))
			{
				await _imageRepository.RemoveAsync(image);
			}
			else
			{
				throw new ForbiddenException("You do not have sufficient access to remove image.");
			}
		}

		/// <summary>
		/// Возвращает флаг валидности файла картинки.
		/// </summary>
		/// <param name="image">Файл картинки.</param>
		/// <returns>Флаг валидности.</returns>
		private bool IsValid(File image) =>
			image?.Length > 0 &&
			image.Length <= long.Parse(_configuration[ImageService<User>.MAX_IMAGE_LENGTH_SECTION]) &&
			AllowedContentTypes.Contains(image.ContentType);

		/// <summary>
		/// Возвращает флаг наличия у текущего пользователя полных прав на работу с картинками определенного пользователя.
		/// </summary>
		/// <param name="target_user_id">Идентификатор пользователя, относительно которого проверяются права.</param>
		/// <returns>Флаг наличия полных прав на работу с картинками.</returns>
		private async Task<bool> HasFullAccessAsync(int target_user_id)
		{
			T current_user = await _userService.GetCurrentAsync();
			return current_user != null &&
				(current_user.Id == target_user_id || await _userService.IsInRoleAsync(current_user, RoleNames.ADMIN));
		}
	}
}