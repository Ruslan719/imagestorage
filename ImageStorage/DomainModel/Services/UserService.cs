﻿using System.Linq;
using System.Threading.Tasks;
using System.Transactions;
using ImageStorage.DomainModel.DataModels;
using ImageStorage.DomainModel.Exceptions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace ImageStorage.DomainModel.Services
{
	/// <summary>
	/// Сервис пользователей.
	/// </summary>
	/// <typeparam name="T">Тип пользователя.</typeparam>
	public class UserService<T> : IUserService<T> where T : IdentityUser<int>,  new()
	{
		/// <summary>
		/// Стандартный объект управления пользователями.
		/// </summary>
		private readonly UserManager<T> _userManager;

		/// <summary>
		/// Сервис доступа к HTTP-контексту.
		/// </summary>
		private readonly IHttpContextAccessor _httpContextAccessor;

		/// <summary>
		/// Инициализирует поля.
		/// </summary>
		/// <param name="user_manager">Стандартный объект управления пользователями.</param>
		/// <param name="http_context_accessor">Сервис доступа к HTTP-контексту.</param>
		public UserService(UserManager<T> user_manager, IHttpContextAccessor http_context_accessor)
		{
			_userManager = user_manager;
			_httpContextAccessor = http_context_accessor;
		}

		/// <summary>
		/// Создает пользователя.
		/// Назначать роли может только администратор.
		/// </summary>
		/// <param name="user_credential">Учетные данные пользователя.</param>
		/// <param name="role_names">Названия ролей, которые нужно присвоить пользователю.</param>
		/// <returns>Результат создания.</returns>
		public async Task<IdentityResult> CreateAsync(UserCredential user_credential, params string[] role_names)
		{
			await CheckCreationAsync(role_names);

			using (var transaction = new TransactionScope(TransactionScopeOption.Required,
				new TransactionOptions { IsolationLevel = IsolationLevel.ReadCommitted },
				TransactionScopeAsyncFlowOption.Enabled))
			{
				var new_user = new T {UserName = user_credential.Login};
				IdentityResult result = await _userManager.CreateAsync(new_user, user_credential.Password);
				if (!result.Succeeded)
				{
					return result;
				}

				result = await _userManager.AddToRolesAsync(new_user, role_names);
				transaction.Complete();
				return result;
			}
		}

		/// <summary>
		/// Возвращает текущего авторизованного пользователя.
		/// Если пользователь не авторизован или не определен, вернет <code>null</code>.
		/// </summary>
		/// <returns>Пользователь.</returns>
		public Task<T> GetCurrentAsync() =>
			(_httpContextAccessor.HttpContext?.User?.Identity?.IsAuthenticated ?? false)
				? _userManager.GetUserAsync(_httpContextAccessor.HttpContext.User)
				: Task.FromResult((T)null);

		/// <summary>
		/// Возвращает пользователя.
		/// </summary>
		/// <param name="id">Идентификатор пользователя.</param>
		/// <returns>Пользователь.</returns>
		public Task<T> GetAsync(int id) => _userManager.Users.FirstOrDefaultAsync(u => u.Id == id);

		/// <summary>
		/// Возвращает пользователя.
		/// </summary>
		/// <param name="user_name">Имя пользователя.</param>
		/// <returns>Пользователь.</returns>
		public Task<T> GetAsync(string user_name) => _userManager.FindByNameAsync(user_name);

		/// <summary>
		/// Удаляет пользователя и его ресурсы.
		/// Удалять пользователей может только администратор.
		/// Если удаляемый пользователь не найден, выбрасывает исключение.
		/// </summary>
		/// <param name="id">Идентификатор пользователя.</param>
		/// <returns>Результат удаления.</returns>
		public async Task<IdentityResult> DeleteAsync(int id)
		{
			if (!await CurrentUserIsAdmin())
			{
				throw new ForbiddenException("You do not have sufficient access to delete user.");
			}

			T user = await GetAsync(id);
			if (user == null)
			{
				throw new NotFoundException($"User with id {id} was not found.");
			}

			return await _userManager.DeleteAsync(user);
		}

		/// <summary>
		/// Возвращает флаг, определяющий есть ли у пользователя роль.
		/// </summary>
		/// <param name="user">Пользователь</param>
		/// <param name="role_name">Название роли.</param>
		/// <returns>Флаг, определяющий есть ли у пользователя роль.</returns>
		public Task<bool> IsInRoleAsync(T user, string role_name) => _userManager.IsInRoleAsync(user, role_name);

		/// <summary>
		/// Проверяет возможность создания пользователя, выбрасывая исключение в случае невозможности.
		/// </summary>
		/// <param name="role_names">Названия ролей, которые нужно присвоить пользователю.</param>
		private async Task CheckCreationAsync(params string[] role_names)
		{
			if (role_names.Any())
			{
				if (!await CurrentUserIsAdmin())
				{
					throw new ForbiddenException("You do not have sufficient access to assign roles to user.");
				}
			}
		}

		/// <summary>
		/// Возвращает флаг того, что текущий пользователь является администратором.
		/// </summary>
		/// <returns>Флаг того, что текущий пользователь является администратором.</returns>
		private async Task<bool> CurrentUserIsAdmin()
		{
			T current_user = await GetCurrentAsync();
			return current_user != null && await IsInRoleAsync(current_user, RoleNames.ADMIN);
		}
	}
}