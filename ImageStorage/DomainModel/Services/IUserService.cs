﻿using ImageStorage.DomainModel.DataModels;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;

namespace ImageStorage.DomainModel.Services
{
	/// <summary>
	/// Интерфейс сервиса пользователей.
	/// </summary>
	/// <typeparam name="T">Тип пользователя.</typeparam>
	public interface IUserService<T> where T : IdentityUser<int>, new()
	{
		/// <summary>
		/// Создает пользователя.
		/// </summary>
		/// <param name="user_credential">Учетные данные пользователя.</param>
		/// <param name="roles">Роли, которые нужно присвоить пользователю.</param>
		/// <returns>Результат создания.</returns>
		Task<IdentityResult> CreateAsync(UserCredential user_credential, params string[] roles);

		/// <summary>
		/// Возвращает текущего авторизованного пользователя.
		/// </summary>
		/// <returns>Пользователь.</returns>
		Task<T> GetCurrentAsync();

		/// <summary>
		/// Возвращает пользователя.
		/// </summary>
		/// <param name="id">Идентификатор пользователя.</param>
		/// <returns>Пользователь.</returns>
		Task<T> GetAsync(int id);

		/// <summary>
		/// Возвращает пользователя.
		/// </summary>
		/// <param name="user_name">Имя пользователя.</param>
		/// <returns>Пользователь.</returns>
		Task<T> GetAsync(string user_name);

		/// <summary>
		/// Удаляет пользователя и его ресурсы.
		/// </summary>
		/// <param name="id">Идентификатор пользователя.</param>
		/// <returns>Результат удаления.</returns>
		Task<IdentityResult> DeleteAsync(int id);

		/// <summary>
		/// Возвращает флаг, определяющий есть ли у пользователя роль.
		/// </summary>
		/// <param name="user">Пользователь</param>
		/// <param name="role_name">Название роли.</param>
		/// <returns>Флаг, определяющий есть ли у пользователя роль.</returns>
		Task<bool> IsInRoleAsync(T user, string role_name);
	}
}