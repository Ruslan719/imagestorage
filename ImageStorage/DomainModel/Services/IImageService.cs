﻿using ImageStorage.DomainModel.DataModels;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;

namespace ImageStorage.DomainModel.Services
{
	/// <summary>
	/// Интерфейс сервиса картинок.
	/// </summary>
	/// <typeparam name="T">Тип пользователя.</typeparam>
	public interface IImageService<T> where T : IdentityUser<int>, new()
	{
		/// <summary>
		/// Возвращает картинку.
		/// </summary>
		/// <param name="id">Идентификатор картинки.</param>
		/// <returns>Картинка.</returns>
		Task<Image> GetAsync(int id);

		/// <summary>
		/// Возвращает список картинок, принадлежащих пользователю.
		/// </summary>
		/// <param name="user_id">Идентификатор пользователя.</param>
		/// <returns>Список картинок.</returns>
		Task<List<Image>> GetByUserIdAsync(int user_id);

		/// <summary>
		/// Добавляет картинки.
		/// </summary>
		/// <param name="image_files">Список файлов.</param>
		Task AddRangeAsync(List<File> image_files);

		/// <summary>
		/// Изменяет картинку.
		/// Если картинки с указанным идентификатором нет, выбрасывает исключение.
		/// </summary>
		/// <param name="id">Идентификатор картинки.</param>
		/// <param name="is_public">Имеет публичный доступ.</param>
		Task UpdateAsync(int id, bool is_public);

		/// <summary>
		/// Удаляет картинку.
		/// Если картинки с указанным идентификатором нет, не выбрасывает исключение.
		/// </summary>
		/// <param name="id">Идентификатор картинки.</param>
		Task RemoveAsync(int id);
	}
}