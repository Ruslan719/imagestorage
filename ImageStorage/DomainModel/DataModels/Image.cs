﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace ImageStorage.DomainModel.DataModels
{
	/// <summary>
	/// Картинка.
	/// </summary>
	public class Image : File
	{
		/// <summary>
		/// Идентификатор.
		/// </summary>
		public int Id { get; set; }

		/// <summary>
		/// Имеет публичный доступ.
		/// </summary>
		public bool IsPublic { get; set; }

		/// <summary>
		/// Дата добавления.
		/// </summary>
		public DateTime InsertDt { get; set; }

		/// <summary>
		/// Идентификатор пользователя, которому принадлежит картинка.
		/// </summary>
		[ForeignKey(nameof(DataModels.User))]
		public int UserId { get; set; }

		/// <summary>
		/// Пользователь, которому принадлежит картинка.
		/// </summary>
		public User User { get; set; }
	}
}
