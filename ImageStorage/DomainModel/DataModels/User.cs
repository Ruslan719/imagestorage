﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Identity;

namespace ImageStorage.DomainModel.DataModels
{
	/// <summary>
	/// Пользователь.
	/// </summary>
	public class User : IdentityUser<int>
	{
		/// <summary>
		/// Коллекция картинок, которые принадлежат пользователю.
		/// </summary>
		public ICollection<Image> Images { get; set; }
	}
}