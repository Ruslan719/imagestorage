﻿namespace ImageStorage.DomainModel.DataModels
{
	/// <summary>
	/// Названия ролей.
	/// </summary>
	public static class RoleNames
	{
		/// <summary>
		/// Название роли администратора.
		/// </summary>
		public const string ADMIN = "Admin";
	}
}