﻿namespace ImageStorage.DomainModel.DataModels
{
	/// <summary>
	/// Учетные данные пользователя.
	/// </summary>
	public class UserCredential
	{
		/// <summary>
		/// Логин.
		/// </summary>
		public string Login { get; set; }

		/// <summary>
		/// Пароль.
		/// </summary>
		public string Password { get; set; }

		/// <summary>
		/// Пользователь является администратором.
		/// </summary>
		public bool IsAdmin { get; set; }
	}
}