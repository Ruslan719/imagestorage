﻿using System.ComponentModel.DataAnnotations;

namespace ImageStorage.DomainModel.DataModels
{
	/// <summary>
	/// Файл.
	/// </summary>
	public class File
	{
		/// <summary>
		/// Данные файла, закодированные стандартом Base64.
		/// </summary>
		[Required]
		public string Base64Data { get; set; }

		/// <summary>
		/// Название.
		/// </summary>
		[Required]
		public string Name { get; set; }

		/// <summary>
		/// Тип содержимого.
		/// </summary>
		public ContentType ContentType { get; set; }

		/// <summary>
		/// Размер в байтах.
		/// </summary>
		public long Length { get; set; }
	}
}