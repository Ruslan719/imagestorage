﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Runtime.Serialization;

namespace ImageStorage.DomainModel.DataModels
{
	/// <summary>
	/// Тип содержимого.
	/// </summary>
	[JsonConverter(typeof(StringEnumConverter))]
	public enum ContentType
	{
		/// <summary>
		/// Неизвестный тип.
		/// </summary>
		Unknown = 0,

		/// <summary>
		/// JPEG рисунок.
		/// </summary>
		[EnumMember(Value = "image/jpeg")]
		ImageJpeg = 2,

		/// <summary>
		/// PNG рисунок.
		/// </summary>
		[EnumMember(Value = "image/png")]
		ImagePng = 3,

		/// <summary>
		/// BMP рисунок.
		/// </summary>
		[EnumMember(Value = "image/bmp")]
		ImageBmp = 4
	}
}