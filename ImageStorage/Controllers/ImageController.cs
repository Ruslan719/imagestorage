﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using ImageStorage.ApiModel;
using ImageStorage.DomainModel.DataModels;
using ImageStorage.DomainModel.Exceptions;
using ImageStorage.DomainModel.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using File = ImageStorage.DomainModel.DataModels.File;

namespace ImageStorage.Controllers
{
	/// <summary>
	/// API картинок.
	/// </summary>
	[Route("api/[controller]")]
	[ApiController]
	[Authorize]
	public class ImageController : ControllerBase
	{
		/// <summary>
		/// Сервис картинок.
		/// </summary>
		private readonly IImageService<User> _imageService;

		/// <summary>
		/// Точка доступа к конфигурации.
		/// </summary>
		private readonly IConfiguration _configuration;
		
		/// <summary>
		/// Максимальный размер тела запроса для добавления картинок в байтах.
		/// </summary>
		private const long ADD_REQUEST_BODY_SIZE_LIMIT = 40 * 1024 * 1024;

		/// <summary>
		/// Инициализирует поля.
		/// </summary>
		/// <param name="image_service">Сервис картинок.</param>
		/// <param name="configuration">Точка доступа к конфигурации.</param>
		public ImageController(IImageService<User> image_service, IConfiguration configuration)
		{
			_imageService = image_service;
			_configuration = configuration;
		}

		/// <summary>
		/// Возвращает список картинок, принадлежащих пользователю.
		/// Если не указан идентификатор текущего пользователя, вернет список публичных картинок. 
		/// </summary>
		/// <param name="user_id">Идентификатор пользователя.</param>
		/// <returns>Список картинок.</returns>
		[HttpGet("{user_id}")]
		[AllowAnonymous]
		// TODO Paginating.
		public async Task<List<Image>> GetByUserId(int user_id)
		{
			List<Image> result = await _imageService.GetByUserIdAsync(user_id);
			result.ForEach(i => i.User = null);
			return result;
		}

		/// <summary>
		/// Добавляет картинки текущему авторизованному пользователю.
		/// Все файлы должны быть корректными.
		/// </summary>
		/// <param name="images">Список файлов картинок.</param>
		/// <returns>Результат выполнения: 200 - успех, 400 - передан некорректный файл, 404/0 - превышен допустимый размер тела запроса.</returns>
		[HttpPost]
		[RequestSizeLimit(ADD_REQUEST_BODY_SIZE_LIMIT)]
		public async Task<IActionResult> Add(IList<IFormFile> images)
		{
			List<File> image_files = (await Task.WhenAll(GetImageFiles(images)))
				.ToList();

			try
			{
				await _imageService.AddRangeAsync(image_files);
			}
			catch (InvalidImageException)
			{
				return BadRequest();
			}

			return Ok();
		}

		/// <summary>
		/// Изменяет картинку.
		/// Если картинки с указанным идентификатором нет, выбрасывает исключение.
		/// Изменить картинку может только автор.
		/// </summary>
		/// <param name="id">Идентификатор картинки.</param>
		/// <param name="is_public">Имеет публичный доступ.</param>
		/// <returns>Результат выполнения: 200 - успех, 403 - текущий пользователь не может изменить картинку, 404 - картинка не найдена.</returns>
		[HttpPut("{id}/{is_public}")]
		public async Task<IActionResult> Update(int id, bool is_public)
		{
			try
			{
				await _imageService.UpdateAsync(id, is_public);
			}
			catch (NotFoundException)
			{
				return NotFound();
			}
			catch (ForbiddenException)
			{
				return Forbid();
			}

			return Ok();
		}

		/// <summary>
		/// Удаляет картинку.
		/// Если картинки с указанным идентификатором нет, не возвращает ошибочный результат.
		/// </summary>
		/// <param name="id">Идентификатор картинки.</param>
		/// <returns>Результат выполнения: 200 - успех, 403 - текущий пользователь не может удалить картинку.</returns>
		[HttpDelete("{id}")]
		public async Task<IActionResult> Remove(int id)
		{
			try
			{
				await _imageService.RemoveAsync(id);
			}
			catch (ForbiddenException)
			{
				return Forbid();
			}
			
			return Ok();
		}

		/// <summary>
		/// Возвращает данные об операциях над картинками.
		/// </summary>
		/// <returns>Данные об операциях над картинками.</returns>
		[HttpOptions]
		public Task<ImageOptions> GetOptions() =>
			Task.FromResult(new ImageOptions
			{
				AllowedContentTypes = ImageService<User>.AllowedContentTypes.ToList(),
				AddRequestBodySizeLimit = ADD_REQUEST_BODY_SIZE_LIMIT,
				MaxImageLength = long.Parse(_configuration[ImageService<User>.MAX_IMAGE_LENGTH_SECTION])
			});

		/// <summary>
		/// Преобразует передаваемые файлы в файлы картинок.
		/// </summary>
		/// <param name="images">Список файлов.</param>
		/// <returns>Перечисление файлов картинок.</returns>
		private IEnumerable<Task<File>> GetImageFiles(IList<IFormFile> images)
		{
			return images.Select(async i =>
			{
				using (var ms = new MemoryStream())
				using (var s = i.OpenReadStream())
				{
					await s.CopyToAsync(ms);

					ContentType content_type;
					try
					{
						content_type = JsonConvert.DeserializeObject<ContentType>($"\"{i.ContentType}\"");
					}
					catch
					{
						content_type = ContentType.Unknown;
					}

					return new File
					{
						Base64Data = Convert.ToBase64String(ms.ToArray()),
						Length = i.Length,
						ContentType = content_type,
						Name = i.FileName
					};
				}
			});
		}
	}
}