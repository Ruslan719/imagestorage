﻿using System.Threading.Tasks;
using ImageStorage.DomainModel.DataModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using SignInResult = Microsoft.AspNetCore.Identity.SignInResult;

namespace ImageStorage.Controllers
{
	/// <summary>
	/// API сессий.
	/// </summary>
	[Route("api/[controller]")]
	[ApiController]
	[AllowAnonymous]
	public class SessionController : ControllerBase
	{
		/// <summary>
		/// Сервис авторизации.
		/// </summary>
		private readonly SignInManager<User> _signInManager;

		/// <summary>
		/// Инициализирует поля.
		/// </summary>
		/// <param name="sign_manager">Сервис авторизации.</param>
		public SessionController(SignInManager<User> sign_manager)
		{
			_signInManager = sign_manager;
		}

		/// <summary>
		/// Создает сессию, авторизуя пользователя. Если уже есть открытая сессия, заменяет её.
		/// </summary>
		/// <param name="user_credential">Учетные данные пользователя.</param>
		/// <returns>Результат выполнения: 200 - успех, 401 - неверные данные, 403 - авторизация заблокирована.</returns>
		[HttpPost]
		public async Task<IActionResult> Create(UserCredential user_credential)
		{
			SignInResult result = await _signInManager.PasswordSignInAsync(user_credential.Login,
				user_credential.Password, false, true);

			if (result.IsLockedOut)
			{
				return Forbid();
			}

			if (result.Succeeded)
			{
				return Ok();
			}
			
			return Unauthorized();
		}

		/// <summary>
		/// Закрывает сессию. Если нету открытой сессии, не выбрасывает исключение.
		/// </summary>
		[HttpDelete]
		public Task Close() => _signInManager.SignOutAsync();
	}
}