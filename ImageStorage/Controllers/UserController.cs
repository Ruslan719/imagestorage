﻿using System.Linq;
using System.Threading.Tasks;
using ImageStorage.ApiModel;
using ImageStorage.DomainModel.DataModels;
using ImageStorage.DomainModel.Exceptions;
using ImageStorage.DomainModel.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace ImageStorage.Controllers
{
	/// <summary>
	/// API пользователей.
	/// </summary>
	[Route("api/[controller]")]
	[ApiController]
	// TODO Change password, user list.
	public class UserController : ControllerBase
	{
		/// <summary>
		/// Сервис авторизации.
		/// </summary>
		private readonly SignInManager<User> _signInManager;

		/// <summary>
		/// Сервис пользователей.
		/// </summary>
		private readonly IUserService<User> _userService;

		/// <summary>
		/// Инициализирует поля.
		/// </summary>
		/// <param name="sign_manager">Сервис авторизации.</param>
		/// <param name="user_service">Сервис пользователей.</param>
		public UserController(SignInManager<User> sign_manager, IUserService<User> user_service)
		{
			_signInManager = sign_manager;
			_userService = user_service;
		}

		/// <summary>
		/// Регистрирует (создает) пользователя. В случае успеха авторизует.
		/// </summary>
		/// <param name="user_credential">Учетные данные пользователя.</param>
		/// <returns>
		/// Результат выполнения: 200 - успех, 400 - неуспех с текстом ошибки, 
		/// 403 - текущий пользователь не может зарегистрировать пользователя с заданными правами.
		/// </returns>
		[HttpPost]
		public async Task<IActionResult> Register(UserCredential user_credential)
		{
			string[] roles = user_credential.IsAdmin ? new[] { RoleNames.ADMIN } : new string[0];
			IdentityResult result;
			try
			{
				result = await _userService.CreateAsync(user_credential, roles);
			}
			catch (ForbiddenException)
			{
				return Forbid();
			}

			if (!result.Succeeded)
			{
				string error_message = string.Join(" ", result.Errors.Select(e => e.Description));
				return BadRequest(error_message);
			}

			if (!user_credential.IsAdmin)
			{
				await _signInManager.SignInAsync(await _userService.GetAsync(user_credential.Login), false);
			}

			return Ok();
		}

		/// <summary>
		/// Возвращает информацию о текущем авторизованном пользователе.
		/// Если пользователь не авторизован или не определен, вернет <code>null</code>.
		/// </summary>
		/// <returns>Информация о пользователе.</returns>
		[HttpGet]
		public async Task<UserInfo> GetCurrentUserInfo()
		{
			User user = await _userService.GetCurrentAsync();

			return await GetUserInfoAsync(user);
		}

		/// <summary>
		/// Возвращает информацию о пользователе.
		/// Если пользователь не найден, вернет <code>null</code>.
		/// </summary>
		/// <param name="id">Идентификатор пользователя.</param>
		/// <returns>Информация о пользователе.</returns>
		[HttpGet("{id}")]
		public async Task<UserInfo> GetUserInfo(int id)
		{
			User user = await _userService.GetAsync(id);

			return await GetUserInfoAsync(user);
		}

		/// <summary>
		/// Удаляет пользователя и его ресурсы.
		/// </summary>
		/// <param name="id">Идентификатор пользователя.</param>
		/// <returns>
		/// Результат выполнения: 200 - успех, 400 - неуспех с текстом ошибки, 
		/// 403 - у текущего пользователя нет прав на удаление, 404 - пользователь не найден.
		/// </returns>
		[HttpDelete("{id}")]
		[Authorize(Roles = RoleNames.ADMIN)]
		public async Task<IActionResult> Delete(int id)
		{
			IdentityResult result;
			try
			{
				result = await _userService.DeleteAsync(id);
			}
			catch (ForbiddenException)
			{
				return Forbid();
			}
			catch (NotFoundException)
			{
				return NotFound();
			}

			if (!result.Succeeded)
			{
				string error_message = string.Join(" ", result.Errors.Select(e => e.Description));
				return BadRequest(error_message);
			}

			return Ok();
		}

		/// <summary>
		/// Возвращает информацию о пользователе не основе его сущности.
		/// </summary>
		/// <param name="user">Пользователь.</param>
		/// <returns>Информация о пользователе.</returns>
		private async Task<UserInfo> GetUserInfoAsync(User user)
		{
			if (user == null)
			{
				return null;
			}

			return new UserInfo
			{
				Id = user.Id,
				Name = user.UserName,
				IsAdmin = await _userService.IsInRoleAsync(user, RoleNames.ADMIN)
			};
		}
	}
}