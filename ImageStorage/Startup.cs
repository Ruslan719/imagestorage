﻿using System;
using ImageStorage.DomainModel.DataModels;
using ImageStorage.DomainModel.DbContexts;
using ImageStorage.DomainModel.Repositories;
using ImageStorage.DomainModel.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json.Serialization;

namespace ImageStorage
{
	public class Startup
	{
		public Startup(IConfiguration configuration)
		{
			Configuration = configuration;
		}

		public IConfiguration Configuration { get; }
		
		public void ConfigureServices(IServiceCollection services)
		{
			services.AddScoped<IImageService<User>, ImageService<User>>();
			services.AddScoped<IImageRepository, ImageRepository>();
			services.AddScoped<IUserService<User>, UserService<User>>();

			services.AddEntityFrameworkSqlServer()
				.AddDbContext<MainIdentityContext>(option => option.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));

			services.AddIdentity<User, IdentityRole<int>>()
				.AddEntityFrameworkStores<MainIdentityContext>();

			services.Configure<IdentityOptions>(options =>
			{
				options.Password.RequireDigit = true;
				options.Password.RequireLowercase = false;
				options.Password.RequireUppercase = false;
				options.Password.RequireNonAlphanumeric = false;
				options.Password.RequiredLength = 6;
				
				options.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(5);
				options.Lockout.MaxFailedAccessAttempts = 5;
				
				options.User.AllowedUserNameCharacters =
					"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-._@+";
			});

			services.ConfigureApplicationCookie(options =>
			{
				options.Cookie.HttpOnly = true;
				options.ExpireTimeSpan = TimeSpan.FromHours(1);

				options.LoginPath = "/index.html";
				options.AccessDeniedPath = "/index.html";
				options.SlidingExpiration = true;
			});

			services.AddMvc()
				.SetCompatibilityVersion(CompatibilityVersion.Version_2_1)
				// Меняем регистр возвращаемых полей.
				.AddJsonOptions(options => options.SerializerSettings.ContractResolver = new DefaultContractResolver());
		}
		
		public void Configure(IApplicationBuilder app, IHostingEnvironment env)
		{
			if (env.IsDevelopment())
			{
				app.UseDeveloperExceptionPage();
			}

			app.UseDefaultFiles();
			// Используем статичные страницы интерфейса.
			app.UseStaticFiles();
			app.UseAuthentication();
			app.UseMvc();
		}
	}
}
