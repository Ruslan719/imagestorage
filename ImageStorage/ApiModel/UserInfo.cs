﻿namespace ImageStorage.ApiModel
{
	/// <summary>
	/// Публичная информация о пользователе.
	/// </summary>
	public class UserInfo
	{
		/// <summary>
		/// Идентификатор.
		/// </summary>
		public int Id { get; set; }

		/// <summary>
		/// Имя.
		/// </summary>
		public string Name { get; set; }

		/// <summary>
		/// Пользователь является администратором.
		/// </summary>
		public bool IsAdmin { get; set; }
	}
}