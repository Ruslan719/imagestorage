﻿using System.Collections.Generic;
using ImageStorage.DomainModel.DataModels;

namespace ImageStorage.ApiModel
{
	/// <summary>
	/// Данные об операциях над картинками.
	/// </summary>
	public class ImageOptions
	{
		/// <summary>
		/// Максимальный размер тела запроса для добавления картинок в байтах.
		/// </summary>
		public long AddRequestBodySizeLimit { get; set; }

		/// <summary>
		/// Максимальный размер картинки в байтах.
		/// </summary>
		public long MaxImageLength { get; set; }

		/// <summary>
		/// Допустимые типы картинок.
		/// </summary>
		public List<ContentType> AllowedContentTypes { get; set; }
	}
}