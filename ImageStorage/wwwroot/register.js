$(document).ready(function () {
	$("#register_form").submit(function (e) {
		e.preventDefault();

		var serialized_form = $(this).serializeJSON();
		$(this).find(":submit").prop("disabled", true);
		$.ajax({
			type: "POST",
			url: "/api/user",
			contentType: "application/json",
			data: serialized_form,
			error: function (jqXHR) {
				if (jqXHR.status == 400) {
					alert("Error: " + jqXHR.responseText);
				} else if (jqXHR.status == 403) {
					alert("You do not have sufficient access to register administrators!");
				} else {
					alert("Error!");
				}

				$("#register_form").find(":submit").prop("disabled", false);
			},
			success: function () {
				window.location.href = "/index.html";
			}
		});
	});

	$.ajax({
		type: "GET",
		url: "/api/user",
		error: function () {
			alert("Error!");
		},
		success: function (user) {
			if (user && user.IsAdmin) {
				$("#register_as_admin_field").show();
			}
		}
	});
});