var current_user;

$(document).ready(function () {
	$("#sign_out_button").click(function (e) {
		e.preventDefault();

		$("#sign_out_button").prop("disabled", true);
		$.ajax({
			type: "DELETE",
			url: "/api/session",
			error: function () {
				alert("Error!");
				$("#sign_out_button").prop("disabled", false);
			},
			success: function () {
				current_user = null;
				$("#images").empty();
				$("#images_block").hide();
				$("#current_user_images_button").hide();
				$("#delete_user_button").hide();
				$("#sign_out_button").prop("disabled", false)
					.hide();
				$("#add_image_form").hide();
				$("#sign_in_form").show();
			}
		});
	});

	$("#add_image_form").submit(function (e) {
		e.preventDefault();

		var form_data = new FormData();
		Array.from($("#images_input")[0].files).forEach(function (image) {
			form_data.append("images", image);
		});
		$(this).find(":submit").prop("disabled", true);

		$.ajax({
			type: "POST",
			url: "/api/image",
			data: form_data,
			processData: false,
			contentType: false,
			error: function (jqXHR) {
				if (jqXHR.status == 401) {
					alert("Sign in first!");
				} else if (jqXHR.status == 400) {
					alert("There was incorrect image!");
				} else if (jqXHR.status == 0 || jqXHR.status == 413 || jqXHR.status == 404) {
					alert("Allowed total size exceeded!");
				} else {
					alert("Error!");
				}

				$("#add_image_form").find(":submit").prop("disabled", false);
			},
			success: function () {
				window.location.reload(true);
			}
		});
	});

	$("#sign_in_form").submit(function (e) {
		e.preventDefault();

		var serialized_form = $(this).serializeJSON();
		$(this).find(":submit").prop("disabled", true);
		$.ajax({
			type: "POST",
			url: "/api/session",
			contentType: "application/json",
			data: serialized_form,
			error: function (jqXHR) {
				if (jqXHR.status == 401) {
					alert("Incorrect credential!");
				} else if (jqXHR.status == 403) {
					alert("User was locked out!");
				} else {
					alert("Error!");
				}

				$("#sign_in_form").find(":submit").prop("disabled", false);
			},
			success: function () {
				window.location.reload(true);
			}
		});
	});

	$("#user_id_textbox").on('input', function (e) {
		e.preventDefault();

		if ($("#user_id_textbox").val()) {
			$("#user_images_button").prop("disabled", false);
			$("#delete_user_button").prop("disabled", false);
		} else {
			$("#user_images_button").prop("disabled", true);
			$("#delete_user_button").prop("disabled", true);
		}
	});

	$("#current_user_images_button").click(function (e) {
		e.preventDefault();

		$("#current_user_images_button").prop("disabled", true);
		$("#images_block_title").text("Images of " + current_user.Name);
		loadImages(current_user.Id);
	});

	$("#delete_user_button").click(function (e) {
		e.preventDefault();

		$.confirm({
			title: 'Are you sure you want to delete user?',
			content: '',
			buttons: {
				confirm: function () {
					$("#delete_user_button").prop("disabled", true);
					var user_id = $("#user_id_textbox").val();
					$.ajax({
						type: "DELETE",
						url: "/api/user/" + user_id,
						error: function (jqXHR) {
							if (jqXHR.status == 400) {
								alert("Error: " + jqXHR.responseText);
							} else if (jqXHR.status == 401 || jqXHR.status == 403) {
								alert("You do not have sufficient access to delete user!");
							} else if (jqXHR.status == 404) {
								alert('User with id "' + user_id + '" was not found.');
							} else {
								alert("Error!");
							}
							$("#delete_user_button").prop("disabled", false);
						},
						success: function () {
							alert("User with id " + user_id + " was deleted.");
							$("#delete_user_button").prop("disabled", false);
						}
					});
				},
				cancel: function () {
				}
			}
		});
	});

	$("#user_images_button").click(function (e) {
		e.preventDefault();

		$("#current_user_images_button").prop("disabled", false);
		var user_id = $("#user_id_textbox").val();

		$.ajax({
			type: "GET",
			url: "/api/user/" + user_id,
			error: function () {
				alert("Error!");
			},
			success: function (user) {
				if (user) {
					$("#images_block_title").text("Images of " + user.Name);
					loadImages(user.Id);
				} else {
					alert('User with id "' + user_id + '" was not found.');
				}
			}
		});
	});

	$.ajax({
		type: "GET",
		url: "/api/user",
		error: function () {
			alert("Error!");
		},
		success: function (user) {
			if (user) {
				current_user = user;
				$("#current_user_images_button").show();
				$("#sign_out_button").show();
				$("#add_image_form").show();
				$("#images_block_title").text("Images of " + current_user.Name);
				if (current_user.IsAdmin) {
					$("#delete_user_button").show();
				}
				loadImages(current_user.Id);
				loadOptions();
			} else {
				$("#sign_in_form").show();
			}
		}
	});

	function loadImages(user_id) {
		$("#images").empty();
		$("#loading_text").show();
		$("#images_block").show();
		$.ajax({
			type: "GET",
			url: "/api/image/" + user_id,
			error: function () {
				alert("Error!");
				$("#loading_text").hide();
				$("#images_block").hide();
				$("#current_user_images_button").prop("disabled", false);
			},
			success: function (images) {
				$("#images").append("<hr>");
				images.forEach(function (image) {
					var element = $('<div class="image_block"></div>');
					element.append('<img src="data:image/;base64, ' + image.Base64Data + '" style="width: 30%" />');

					if (current_user && (current_user.Id == user_id || current_user.IsAdmin)) {
						var delete_button = $('<input type="button" value="Delete" />');
						delete_button.click(function () {
							$.confirm({
								title: 'Are you sure you want to delete image?',
								content: '',
								buttons: {
									confirm: function () {
										deleteImage(delete_button, image.Id);
									},
									cancel: function () {
									}
								}
							});
						});

						var change_access_button = $('<input type="button" value="' +
							(image.IsPublic ? 'Disable' : 'Enable') + ' public access" />');
						change_access_button.click(function () {
							changeAccess(change_access_button, image.Id, !image.IsPublic);
						});
						element.append(delete_button);
						element.append(change_access_button);
					}

					element.append("<hr>");
					$("#images").append(element);
				});
				$("#loading_text").hide();
			}
		});
	}

	function loadOptions() {
		$.ajax({
			type: "OPTIONS",
			url: "/api/image",
			error: function () {
				alert("Error!");
			},
			success: function (image_options) {
				$("#images_input").attr("accept", image_options.AllowedContentTypes.join(", "));
				$("#images_input").prop("disabled", false);
				$("#images_info").text("Maximum image size: " +
					image_options.MaxImageLength / 1024 / 1024 + " MB. Maximum total size: " +
					image_options.AddRequestBodySizeLimit / 1024 / 1024 + " MB.");
			}
		});
	}

	function deleteImage(delete_button, image_id) {
		delete_button.prop("disabled", true);
		$.ajax({
			type: "DELETE",
			url: "/api/image/" + image_id,
			error: function (jqXHR) {
				if (jqXHR.status == 401) {
					alert("Sign in first!");
				} else if (jqXHR.status == 403) {
					alert("You can not remove this image!");
				} else {
					alert("Error!");
				}
				delete_button.prop("disabled", false);
			},
			success: function () {
				delete_button.closest(".image_block")
					.remove();
			}
		});
	}

	function changeAccess(change_access_button, image_id, is_public) {
		change_access_button.prop("disabled", true);
		$.ajax({
			type: "PUT",
			url: "/api/image/" + image_id + "/" + is_public,
			error: function (jqXHR) {
				if (jqXHR.status == 401) {
					alert("Sign in first!");
				} else if (jqXHR.status == 403) {
					alert("You can not update this image!");
				} else if (jqXHR.status == 404) {
					alert("Image was not found!");
				} else {
					alert("Error!");
				}
				change_access_button.prop("disabled", false);
			},
			success: function () {
				change_access_button.val((is_public ? 'Disable' : 'Enable') + ' public access');
				change_access_button.off("click")
					.click(function() {
						changeAccess(change_access_button, image_id, !is_public);
					});
				change_access_button.prop("disabled", false);
			}
		});
	}
});