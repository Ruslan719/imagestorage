﻿using ImageStorage.DomainModel.DataModels;
using ImageStorage.DomainModel.Repositories;
using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ImageStorage.Tests.DomainModel.Repositories
{
	/// <summary>
	/// Тесты репозитория картинок.
	/// </summary>
	public class ImageRepositoryTests : RepositoryTests
	{
		#region Fields

		/// <summary>
		/// Идентификатор картинки.
		/// </summary>
		private const int IMAGE_ID1 = -67;

		/// <summary>
		/// Идентификатор картинки.
		/// </summary>
		private const int IMAGE_ID2 = -32;

		/// <summary>
		/// Идентификатор пользователя.
		/// </summary>
		private const int USER_ID1 = -98;

		/// <summary>
		/// Идентификатор пользователя.
		/// </summary>
		private const int USER_ID2 = -11;

		/// <summary>
		/// Репозиторий картинок.
		/// </summary>
		private ImageRepository _imageRepository;

		#endregion

		#region SetUp

		/// <summary>
		/// Инициализирует поля и заполняет хранилище.
		/// </summary>
		[SetUp]
		public void SetUp()
		{
			_imageRepository = new ImageRepository(context);

			context.Images.Add(new Image { Id = IMAGE_ID1, UserId = USER_ID1, Base64Data = "data1" });
			context.Images.Add(new Image { Id = IMAGE_ID2, UserId = USER_ID2, Base64Data = "data2" });
			context.Users.Add(new User { Id = USER_ID1 });
			context.Users.Add(new User { Id = USER_ID2 });
			context.SaveChanges();
		}

		#endregion

		#region Tests

		/// <summary>
		/// Проверяет, что получение картинки возвращает корректный результат, 
		/// если указан существующий идентификатор картинки.
		/// </summary>
		[Test]
		public async Task GetAsync_ExistingImage_ReturnsCorrect()
		{
			Image result = await _imageRepository.GetAsync(IMAGE_ID2);

			Assert.AreEqual(IMAGE_ID2, result.Id);
		}

		/// <summary>
		/// Проверяет, что получение картинки возвращает нулевой результат, 
		/// если указан не существующий идентификатор картинки.
		/// </summary>
		[Test]
		public async Task GetAsync_NotExistingImage_ReturnsNull()
		{
			Image result = await _imageRepository.GetAsync(IMAGE_ID1 + IMAGE_ID2);

			Assert.IsNull(result);
		}

		/// <summary>
		/// Проверяет, что получение списка картинок возвращает корректный результат, 
		/// если указан идентификатор пользователя с картинками.
		/// </summary>
		[Test]
		public async Task GetByUserIdAsync_UserWithImages_ReturnsCorrect()
		{
			IEnumerable<Image> result = await _imageRepository.GetByUserIdAsync(USER_ID1);

			Assert.IsTrue(result.Any(image => image.Id == IMAGE_ID1));
		}

		/// <summary>
		/// Проверяет, что получение списка картинок возвращает пустой результат, 
		/// если указан не существующий идентификатор пользователя.
		/// </summary>
		[Test]
		public async Task GetByUserIdAsync_UserWithoutImages_ReturnsEmpty()
		{
			IEnumerable<Image> result = await _imageRepository.GetByUserIdAsync(USER_ID1 + USER_ID2);

			Assert.IsEmpty(result);
		}

		/// <summary>
		/// Проверяет, что добавление картинок изменяет хранилище.
		/// </summary>
		[Test]
		public async Task AddRangeAsync_NewImage_Adds()
		{
			var new_images = new List<Image>
			{
				new Image { UserId = USER_ID2, Base64Data = "data3" },
				new Image { UserId = USER_ID2, Base64Data = "data4" }
			};

			await _imageRepository.AddRangeAsync(new_images);

			var images = await _imageRepository.GetByUserIdAsync(USER_ID2);

			Assert.IsTrue(new_images.All(images.Contains));
		}

		/// <summary>
		/// Проверяет, что изменение картинки изменяет хранилище.
		/// </summary>
		[Test]
		public async Task UpdateAsync_NewImage_Adds()
		{
			Image image = await _imageRepository.GetAsync(IMAGE_ID2);
			image.UserId = USER_ID1;

			await _imageRepository.UpdateAsync(image);

			Image updated_image = await _imageRepository.GetAsync(IMAGE_ID2);

			Assert.AreEqual(USER_ID1, updated_image.UserId);
		}

		/// <summary>
		/// Проверяет, что удаление картинки изменяет хранилище.
		/// </summary>
		[Test]
		public async Task RemoveAsync_ExistingImage_Removes()
		{
			Image image = await _imageRepository.GetAsync(IMAGE_ID2);

			await _imageRepository.RemoveAsync(image);

			image = await _imageRepository.GetAsync(IMAGE_ID2);

			Assert.IsNull(image);
		}

		#endregion
	}
}