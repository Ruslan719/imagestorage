﻿using ImageStorage.DomainModel.DbContexts;
using Microsoft.EntityFrameworkCore;
using NUnit.Framework;
using System;

namespace ImageStorage.Tests.DomainModel
{
	/// <summary>
	/// Тесты репозитория.
	/// </summary>
	public class RepositoryTests
	{
		/// <summary>
		/// Контекст взаимодействия с БД.
		/// </summary>
		protected MainIdentityContext context;

		/// <summary>
		/// Инициализирует новое хранилище и доступ к нему.
		/// </summary>
		[SetUp]
		public void ContextSetup()
		{
			DbContextOptions<MainIdentityContext> options = new DbContextOptionsBuilder<MainIdentityContext>()
				// На каждый тест свое хранилище.
				.UseInMemoryDatabase(Guid.NewGuid().ToString())
				.Options;

			context = new MainIdentityContext(options);
		}
	}
}