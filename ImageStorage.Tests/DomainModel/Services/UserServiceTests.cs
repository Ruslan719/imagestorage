﻿using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;
using ImageStorage.DomainModel.DataModels;
using ImageStorage.DomainModel.Exceptions;
using ImageStorage.DomainModel.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore.Query.Internal;
using NSubstitute;
using NUnit.Framework;

namespace ImageStorage.Tests.DomainModel.Services
{
	/// <summary>
	/// Тесты сервиса пользователей.
	/// </summary>
	public class UserServiceTests
	{
		#region Fields
		
		/// <summary>
		/// Название роли.
		/// </summary>
		private const string ROLE_NAME = "test_role";

		/// <summary>
		/// Учетные данные пользователя.
		/// </summary>
		private UserCredential _userCredential;

		/// <summary>
		/// Сервис пользователей.
		/// </summary>
		private UserService<User> _userService;

		/// <summary>
		/// Стандартный объект управления пользователями.
		/// </summary>
		private UserManager<User> _userManager;

		/// <summary>
		/// Сервис доступа к HTTP-контексту.
		/// </summary>
		private IHttpContextAccessor _httpContextAccessor;

		/// <summary>
		/// Хранилище связи пользователей с ролями.
		/// </summary>
		private IUserStore<User> _userStore;

		/// <summary>
		/// Сервис валидации паролей.
		/// </summary>
		private IPasswordValidator<User> _passwordValidator;

		#endregion

		#region SetUp

		/// <summary>
		/// Инициализирует поля и заглушки.
		/// </summary>
		[SetUp]
		public void SetUp()
		{
			_userStore = Substitute.For<IUserRoleStore<User>, IUserPasswordStore<User>, IQueryableUserStore<User>>();
			var password_hasher = Substitute.For<IPasswordHasher<User>>();
			_passwordValidator = Substitute.For<IPasswordValidator<User>>();
			_userManager = new UserManager<User>(_userStore,
				null, password_hasher, null, null, null, null, null, null);
			_httpContextAccessor = Substitute.For<IHttpContextAccessor>();

			_userService = new UserService<User>(_userManager, _httpContextAccessor);

			password_hasher.HashPassword(Arg.Any<User>(), Arg.Any<string>())
				.Returns("pass_hash");
			_userCredential = new UserCredential { Login = "test_user", Password = "test_pass" };
			_passwordValidator.ValidateAsync(Arg.Any<UserManager<User>>(), Arg.Any<User>(), _userCredential.Password)
				.Returns(IdentityResult.Success);
		}

		#endregion

		#region Tests

		/// <summary>
		/// Проверяет, что создание пользователя выбрасывает исключение в случае, 
		/// когда передана роль и текущий пользователь не определен.
		/// </summary>
		[Test]
		public void CreateAsync_WithRoleAndNoCurrentUser_ThrowsForbiddenException() =>
			Assert.ThrowsAsync<ForbiddenException>(async () => await _userService.CreateAsync(_userCredential, ROLE_NAME));

		/// <summary>
		/// Проверяет, что создание пользователя выбрасывает исключение в случае, 
		/// когда передана роль и текущий пользователь не может назначать роли.
		/// </summary>
		[Test]
		public void CreateAsync_WithRoleAndCurrentUserIsNotAdmin_ThrowsForbiddenException()
		{
			MockIsAdmin(false);

			Assert.ThrowsAsync<ForbiddenException>(async () => await _userService.CreateAsync(new UserCredential(), ROLE_NAME));
		}

		/// <summary>
		/// Проверяет, что создание пользователя возвращает результат успешного выполнения, 
		/// если не передана ни одна роль.
		/// </summary>
		[Test]
		public async Task CreateAsync_WithoutRoleAndNoCurrentUser_SuccessResult()
		{
			_userStore.CreateAsync(Arg.Any<User>(), Arg.Any<CancellationToken>())
				.Returns(IdentityResult.Success);
			_userStore.UpdateAsync(Arg.Any<User>(), Arg.Any<CancellationToken>())
				.Returns(IdentityResult.Success);

			var result = await _userService.CreateAsync(_userCredential);

			Assert.AreEqual(IdentityResult.Success, result);
		}

		/// <summary>
		/// Проверяет, что создание пользователя возвращает результат успешного выполнения, 
		/// если передана роль и текущий пользователь может назначать роли.
		/// </summary>
		[Test]
		public async Task CreateAsync_WithRoleAndValidCurrentUser_SuccessResult()
		{
			MockIsAdmin(true);

			_userStore.CreateAsync(Arg.Any<User>(), Arg.Any<CancellationToken>())
				.Returns(IdentityResult.Success);
			_userStore.UpdateAsync(Arg.Any<User>(), Arg.Any<CancellationToken>())
				.Returns(IdentityResult.Success);

			var result = await _userService.CreateAsync(_userCredential, ROLE_NAME);

			Assert.AreEqual(IdentityResult.Success, result);
		}

		#endregion

		#region DeleteAsync

		/// <summary>
		/// Проверяет, что удаление пользователя выбрасывает исключение в случае, 
		/// когда текущий пользователь не является администратором.
		/// </summary>
		[Test]
		public void DeleteAsync_WithRoleAndCurrentUserIsNotAdmin_ThrowsForbiddenException()
		{
			MockIsAdmin(false);

			Assert.ThrowsAsync<ForbiddenException>(async () => await _userService.DeleteAsync(-7));
		}

		/// <summary>
		/// Проверяет, что удаление пользователя выбрасывает исключение в случае, 
		/// когда указан идентификатор несуществующего пользователя.
		/// </summary>
		[Test]
		public void DeleteAsync_DeletingUserNotFound_ThrowsNotFoundException()
		{
			MockIsAdmin(true);

			((IQueryableUserStore<User>)_userStore).Users
				.Returns(new TestAsyncEnumerable<User>(new User[0]));

			Assert.ThrowsAsync<NotFoundException>(async () => await _userService.DeleteAsync(-81));
		}

		#endregion

		#region Methods/Private

		/// <summary>
		/// Устанавливает текущего пользователя и наличие у него роли администратора.
		/// </summary>
		/// <param name="is_admin">Текущий пользователь является администратором.</param>
		private void MockIsAdmin(bool is_admin)
		{
			var user = new User();
			_httpContextAccessor.HttpContext.User = new ClaimsPrincipal(new ClaimsIdentity(new[]
			{
				new Claim(_userManager.Options.ClaimsIdentity.UserIdClaimType, _userCredential.Login)
			}, "test_auth_type"));

			_userStore.FindByIdAsync(_userCredential.Login, Arg.Any<CancellationToken>())
				.Returns(user);

			((IUserRoleStore<User>)_userStore).IsInRoleAsync(user, RoleNames.ADMIN, Arg.Any<CancellationToken>())
				.Returns(is_admin);
		}

		#endregion
	}

	#region Mock classes

	// https://stackoverflow.com/questions/40476233/how-to-mock-an-async-repository-with-entity-framework-core
	internal class TestAsyncQueryProvider<TEntity> : IAsyncQueryProvider
	{
		private readonly IQueryProvider _inner;

		internal TestAsyncQueryProvider(IQueryProvider inner)
		{
			_inner = inner;
		}

		public IQueryable CreateQuery(Expression expression)
		{
			return new TestAsyncEnumerable<TEntity>(expression);
		}

		public IQueryable<TElement> CreateQuery<TElement>(Expression expression)
		{
			return new TestAsyncEnumerable<TElement>(expression);
		}

		public object Execute(Expression expression)
		{
			return _inner.Execute(expression);
		}

		public TResult Execute<TResult>(Expression expression)
		{
			return _inner.Execute<TResult>(expression);
		}

		public IAsyncEnumerable<TResult> ExecuteAsync<TResult>(Expression expression)
		{
			return new TestAsyncEnumerable<TResult>(expression);
		}

		public Task<TResult> ExecuteAsync<TResult>(Expression expression, CancellationToken cancellationToken)
		{
			return Task.FromResult(Execute<TResult>(expression));
		}
	}

	internal class TestAsyncEnumerable<T> : EnumerableQuery<T>, IAsyncEnumerable<T>, IQueryable<T>
	{
		public TestAsyncEnumerable(IEnumerable<T> enumerable)
			: base(enumerable)
		{ }

		public TestAsyncEnumerable(Expression expression)
			: base(expression)
		{ }

		public IAsyncEnumerator<T> GetEnumerator()
		{
			return new TestAsyncEnumerator<T>(this.AsEnumerable().GetEnumerator());
		}

		IQueryProvider IQueryable.Provider
		{
			get { return new TestAsyncQueryProvider<T>(this); }
		}
	}

	internal class TestAsyncEnumerator<T> : IAsyncEnumerator<T>
	{
		private readonly IEnumerator<T> _inner;

		public TestAsyncEnumerator(IEnumerator<T> inner)
		{
			_inner = inner;
		}

		public void Dispose()
		{
			_inner.Dispose();
		}

		public T Current
		{
			get
			{
				return _inner.Current;
			}
		}

		public Task<bool> MoveNext(CancellationToken cancellationToken)
		{
			return Task.FromResult(_inner.MoveNext());
		}
	}

	#endregion
}