﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ImageStorage.DomainModel.DataModels;
using ImageStorage.DomainModel.Exceptions;
using ImageStorage.DomainModel.Repositories;
using ImageStorage.DomainModel.Services;
using Microsoft.Extensions.Configuration;
using NSubstitute;
using NUnit.Framework;

namespace ImageStorage.Tests.DomainModel.Services
{
	/// <summary>
	/// Тесты сервиса картинок.
	/// </summary>
	public class ImageServiceTests
	{
		#region Fields

		/// <summary>
		/// Идентификатор картинки.
		/// </summary>
		private const int IMAGE_ID = -67;

		/// <summary>
		/// Идентификатор пользователя.
		/// </summary>
		private const int USER_ID1 = -98;

		/// <summary>
		/// Идентификатор пользователя.
		/// </summary>
		private const int USER_ID2 = -11;

		/// <summary>
		/// Сервис картинок.
		/// </summary>
		private ImageService<User> _imageService;

		/// <summary>
		/// Репозиторий картинок.
		/// </summary>
		private IImageRepository _imageRepository;

		/// <summary>
		/// Сервис пользователей.
		/// </summary>
		private IUserService<User> _userService;

		/// <summary>
		/// Точка доступа к конфигурации.
		/// </summary>
		private IConfiguration _configuration;

		#endregion

		#region SetUp

		/// <summary>
		/// Инициализирует поля и заглушки.
		/// </summary>
		[SetUp]
		public void SetUp()
		{
			_imageRepository = Substitute.For<IImageRepository>();
			_userService = Substitute.For<IUserService<User>>();
			_configuration = Substitute.For<IConfiguration>();
			_imageService = new ImageService<User>(_imageRepository, _userService, _configuration);

			_userService.GetCurrentAsync()
				.Returns(new User { Id = USER_ID1 });
			_imageRepository.GetAsync(IMAGE_ID)
				.Returns(new Image { Id = IMAGE_ID, UserId = USER_ID1 });
			_configuration[ImageService<User>.MAX_IMAGE_LENGTH_SECTION].Returns("100000");
		}

		#endregion

		#region GetByUserIdAsync

		/// <summary>
		/// Проверяет, что получение списка картинок возвращает результат с публичными картинками, 
		/// если не указан идентификатор текущего пользователя.
		/// </summary>
		[Test]
		public async Task GetByUserIdAsync_GetFromAnotherUser_ReturnsPublic()
		{
			_imageRepository.GetByUserIdAsync(USER_ID2)
				.Returns(new List<Image>
				{
					new Image { Id = IMAGE_ID - 7 },
					new Image { Id = IMAGE_ID, IsPublic = true }
				});

			List<Image> result = await _imageService.GetByUserIdAsync(USER_ID2);

			Assert.IsTrue(result.Single().IsPublic);
		}

		/// <summary>
		/// Проверяет, что получение списка картинок возвращает результат репозитория, 
		/// если указан идентификатор текущего пользователя.
		/// </summary>
		[Test]
		public async Task GetByUserIdAsync_GetFromCurrentUser_ReturnsFromRepository()
		{
			var expected_result = new List<Image>{ new Image{ Id = IMAGE_ID } };
			_imageRepository.GetByUserIdAsync(USER_ID1)
				.Returns(expected_result);

			List<Image> result = await _imageService.GetByUserIdAsync(USER_ID1);

			Assert.AreEqual(expected_result, result);
		}

		/// <summary>
		/// Проверяет, что получение списка картинок возвращает результат репозитория, 
		/// если текущий пользователь является администратором.
		/// </summary>
		[Test]
		public async Task GetByUserIdAsync_CurrentUserIsAdmin_ReturnsFromRepository()
		{
			var expected_result = new List<Image> { new Image { Id = IMAGE_ID } };
			_imageRepository.GetByUserIdAsync(USER_ID2)
				.Returns(expected_result);

			_userService.IsInRoleAsync(Arg.Is<User>(u => u.Id == USER_ID1), RoleNames.ADMIN)
				.Returns(true);

			List<Image> result = await _imageService.GetByUserIdAsync(USER_ID2);

			Assert.AreEqual(expected_result, result);
		}

		#endregion

		#region RemoveAsync

		/// <summary>
		/// Проверяет, что удаление картинки не выбрасывает исключение в случае, 
		/// когда удаляемой картинки не существует.
		/// </summary>
		[Test]
		public void RemoveAsync_NotExistingImage_DoesNotThrow() =>
			Assert.DoesNotThrowAsync(async () => await _imageService.RemoveAsync(IMAGE_ID - 3));

		/// <summary>
		/// Проверяет, что удаление картинки выбрасывает исключение в случае, 
		/// когда удаляемая картинка не принадлежит текущему пользователю.
		/// </summary>
		[Test]
		public void RemoveAsync_ImageDoesNotBelongToCurrentUser_ThrowsForbiddenException()
		{
			_imageRepository.GetAsync(IMAGE_ID)
				.Returns(new Image { UserId = USER_ID2 });

			Assert.ThrowsAsync<ForbiddenException>(async () => await _imageService.RemoveAsync(IMAGE_ID));
		}

		/// <summary>
		/// Проверяет, что удаление картинки вызывает метод репозитория, если передан идентификатор существующей картинки.
		/// </summary>
		[Test]
		public async Task RemoveAsync_ExistingImage_CallsRepositoryMethod()
		{
			await _imageService.RemoveAsync(IMAGE_ID);

			Received.InOrder(async () => await _imageRepository.RemoveAsync(Arg.Any<Image>()));
		}

		#endregion

		#region AddRangeAsync

		/// <summary>
		/// Проверяет, что добавление картинок выбрасывает исключение в случае, 
		/// когда у файла добавляемой картинки недопустимый тип содержимого.
		/// </summary>
		[Test]
		public void AddRangeAsync_ForbiddenContentType_ThrowsInvalidImageException() =>
			Assert.ThrowsAsync<InvalidImageException>(async () =>
				await _imageService.AddRangeAsync(new List<File> {new File {ContentType = ContentType.Unknown, Length = 14}}));

		/// <summary>
		/// Проверяет, что добавление картинок вызывает метод репозитория, если передан корректный файл картинки.
		/// </summary>
		[Test]
		public async Task AddRangeAsync_ValidImageFile_CallsRepositoryMethod()
		{
			await _imageService.AddRangeAsync(new List<File> {new File {ContentType = ContentType.ImageBmp, Length = 16}});

			Received.InOrder(async () => await _imageRepository.AddRangeAsync(Arg.Any<IEnumerable<Image>>()));
		}

		/// <summary>
		/// Проверяет, что добавление картинок выбрасывает исключение в случае, 
		/// когда текущий пользователь не определен.
		/// </summary>
		[Test]
		public void AddRangeAsync_NoCurrentUser_ThrowsNotFoundException()
		{
			_userService.GetCurrentAsync()
				.Returns((User)null);

			Assert.ThrowsAsync<NotFoundException>(async () =>
				await _imageService.AddRangeAsync(new List<File> {new File {ContentType = ContentType.ImageBmp, Length = 32}}));
		}

		#endregion

		#region UpdateAsync

		/// <summary>
		/// Проверяет, что изменение картинки выбрасывает исключение в случае, 
		/// когда изменяемой картинки не существует.
		/// </summary>
		[Test]
		public void UpdateAsync_NotExistingImage_ThrowsNotFoundException() =>
			Assert.ThrowsAsync<NotFoundException>(async () => await _imageService.UpdateAsync(IMAGE_ID - 3, true));

		/// <summary>
		/// Проверяет, что изменение картинки выбрасывает исключение в случае, 
		/// когда изменяемая картинка не принадлежит текущему пользователю.
		/// </summary>
		[Test]
		public void UpdateAsync_ImageDoesNotBelongToCurrentUser_ThrowsForbiddenException()
		{
			_imageRepository.GetAsync(IMAGE_ID)
				.Returns(new Image { UserId = USER_ID2 });

			Assert.ThrowsAsync<ForbiddenException>(async () => await _imageService.UpdateAsync(IMAGE_ID, true));
		}

		/// <summary>
		/// Проверяет, что удаление картинки вызывает метод репозитория, если передан идентификатор существующей картинки.
		/// </summary>
		[Test]
		public async Task UpdateAsync_ExistingImage_CallsRepositoryMethod()
		{
			bool is_public = true;
			await _imageService.UpdateAsync(IMAGE_ID, is_public);

			Received.InOrder(async () => await _imageRepository.UpdateAsync(Arg.Is<Image>(i => i.Id == IMAGE_ID && i.IsPublic == is_public)));
		}

		#endregion
	}
}